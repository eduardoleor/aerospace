﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AeroSpace
{
    public partial class count3 : ContentPage
    {
        public count3()
        {
            InitializeComponent();
            Webview.Source = "https://le-al.tk/3d/assets-model/count3";

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await progress.ProgressTo(0.9, 900, Easing.SpringIn);
        }

        protected void OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            progress.IsVisible = true;
        }

        protected void OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            progress.IsVisible = false;
        }
    }

}
