﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AeroSpace
{
    public partial class falcon : ContentPage
    {
        public falcon()
        {
            InitializeComponent();
            //Webview.Source = "https://google.com";

        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            //await progress.ProgressTo(0.9, 900, Easing.SpringIn);
        }

        protected void OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            //progress.IsVisible = true;
        }

        protected void OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            //progress.IsVisible = false;
        }

        private async void especifi(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new especificaciones());
        }

    }
}
