﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AeroSpace
{
    public partial class fallos : ContentPage
    {
        public fallos()
        {
            InitializeComponent();
        }

        async void f0_5(object sender, EventArgs e)
        {
            await DisplayAlert("Fallos", "del 1 al 5", "Ver");
            await Navigation.PushAsync(new count1());
        }

        async void f6_11(object sender, EventArgs e)
        {
            await DisplayAlert("Fallos", "del 6 al 11", "Ver");
            await Navigation.PushAsync(new count2());
        }

        async void f12_18(object sender, EventArgs e)
        {
            await DisplayAlert("Fallos", "del 12 al 18", "Ver");
            await Navigation.PushAsync(new count3());
        }

        async void f19_26(object sender, EventArgs e)
        {
            await DisplayAlert("Fallos", "del 19 al 26", "Ver");
            await Navigation.PushAsync(new count4());
        }
    }

}
