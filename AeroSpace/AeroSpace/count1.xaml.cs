﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AeroSpace
{
    public partial class count1 : ContentPage
    {
        public count1()
        {
            InitializeComponent();
            Webview.Source = "https://le-al.tk/3d/assets-model/count1";

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await progress.ProgressTo(0.9, 900, Easing.SpringIn);
        }

        protected void OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            progress.IsVisible = true;
        }

        protected void OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            progress.IsVisible = false;
        }
    }

}
